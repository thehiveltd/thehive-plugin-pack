<?php
/**
 * Plugin Name: Config
 * Description: Website Setup and Customisations
 * Author: Daren Zammit
 * Author URI: https://darenzammit.com
 * Version: 1.0
 */

defined('ABSPATH') || exit;

include_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

add_action('tgmpa_register', function () {
	tgmpa([
		[
			'name' => 'Autoptimize',
			'slug' => 'autoptimize',
		],
		[
			'name' => 'Duplicate Post',
			'slug' => 'duplicate-post',
		],
		[
			'name' => 'Simple Page Ordering',
			'slug' => 'simple-page-ordering',
		],
		[
			'name' => 'Yoast SEO',
			'slug' => 'wordpress-seo',
		],
		[
			'name' => 'Disable Comments',
			'slug' => 'disable-comments',
		],
		[
			'name' => 'GDPR Cookie Consent',
			'slug' => 'cookie-law-info',
		],
		[
			'name' => 'Super Progressive Web Apps',
			'slug' => 'super-progressive-web-apps',
		],
		[
			'name' => 'Gutenberg Ramp',
			'slug' => 'gutenberg-ramp',
		],
		[
			'name'   => 'Elementor Pro',
			'slug'   => 'elementor-pro',
			'source' => dirname(__FILE__) . '/../plugins/elementor-pro.zip',
		],
		[
			'name'   => 'Elementor Extras',
			'slug'   => 'elementor-extras',
			'source' => dirname(__FILE__) . '/../plugins/elementor-extras.zip',
		],
		[
			'name'   => 'Advanced Custom Fields PRO',
			'slug'   => 'advanced-custom-fields-pro',
			'source' => 'http://connect.advancedcustomfields.com/index.php?p=pro&a=download&k=b3JkZXJfaWQ9MzM1MzR8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE0LTA3LTA4IDA3OjM1OjQy',
		],
	]);
});
